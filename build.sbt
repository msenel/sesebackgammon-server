name := "sesebackgammon"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaJpa,
  cache,
  "mysql" % "mysql-connector-java" % "5.1.18",
  "org.springframework" % "spring-webmvc" % "4.0.3.RELEASE",
  "org.springframework" % "spring-context" % "4.0.3.RELEASE",
  "org.springframework" % "spring-orm" % "4.0.3.RELEASE",
  "org.springframework" % "spring-jdbc" % "4.0.3.RELEASE",
  "org.springframework" % "spring-tx" % "4.0.3.RELEASE",
  "org.springframework.data" % "spring-data-mongodb" % "1.4.2.RELEASE",  
  "org.hibernate" % "hibernate-entitymanager" % "4.1.9.Final",
  "c3p0" % "c3p0" % "0.9.1.2"
)     


play.Project.playJavaSettings
