package service;

import java.util.List;
import java.util.Set;

import model.Dice;
import model.User;
import model.dto.DiceDto;
import service.impl.exception.ForbiddenMoveException;

public interface DiceService {

	public List<DiceDto> assembly(Set<Dice> diceList);
	
	public DiceDto assembly(Dice dice);
	
	public List<DiceDto> playDice(long gameId, User user)  throws ForbiddenMoveException;
	
	public void save(Dice dice);
}
