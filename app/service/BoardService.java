package service;

import model.Board;
import model.Game;
import model.Player;
import model.dto.BoardDto;

public interface BoardService {

	public Board initializeBoard(Game game, Player playerWhite, Player playerBlack);
	
	public BoardDto assembly(Board board);
}
