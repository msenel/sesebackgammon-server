package service;

import java.util.List;
import java.util.Set;

import model.Checker;
import model.Player;
import model.Point;
import model.dto.CheckerDto;

public interface CheckerService {
	public Checker createChecker(Player player, Point point);
	
	public List<CheckerDto> assembly(Set<Checker> checkerList);
	
	public CheckerDto assembly(Checker checker);
	
	public Checker save(Checker checker);
}
