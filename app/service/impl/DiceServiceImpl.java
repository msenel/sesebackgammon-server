package service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import model.Dice;
import model.Game;
import model.Movement;
import model.Player;
import model.User;
import model.dao.DiceDao;
import model.dao.MovementDao;
import model.dto.DiceDto;
import model.impl.DiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.DiceService;
import service.GameService;
import service.MovementService;
import service.PlayerService;
import service.impl.exception.ForbiddenMoveException;

@Service
public class DiceServiceImpl implements DiceService {

	@Autowired
	private DiceDao diceDao;

	@Autowired
	private MovementService movementService;

	@Autowired
	private MovementDao movementDao;

	@Autowired
	private PlayerService playerService;

	@Autowired
	private GameService gameService;

	@Override
	public DiceDto assembly(Dice dice) {
		DiceDto diceDto = new DiceDto(dice.getValue(), dice.isPlayed());
		return diceDto;
	}

	@Override
	public List<DiceDto> assembly(Set<Dice> diceList) {
		List<DiceDto> diceDtoList = new ArrayList<DiceDto>();

		for (Dice dice : diceList) {
			diceDtoList.add(assembly(dice));
		}
		return diceDtoList;
	}

	@Override
	public List<DiceDto> playDice(long gameId, User user)
			throws ForbiddenMoveException {
		Game game = gameService.getGame(gameId);
		Player player = playerService.getPlayer(game, user);

		if (game.getEnded() != null) {
			throw new ForbiddenMoveException("Game is over!");
		}
		
		if (game.getColor() != player.getColor()) {
			throw new ForbiddenMoveException("Not your turn. Please wait!");
		}


		Movement lastMovement = movementService.getLastMovement(game);
		
		if(lastMovement != null) {
			for (Dice dice : lastMovement.getDices()) {
				if(!dice.isPlayed()) {
					throw new ForbiddenMoveException("Not allowed to play dice again!");
				}
			}
		}
		
		
		Movement movement = movementService.initializeMovement(player, game,
				new Date());
		Set<Dice> diceList = new HashSet<>();
		Dice firstDice = new DiceImpl();
		firstDice.setMovement(movement);
		firstDice.setValue(getDiceNumber());
		diceDao.save(firstDice);
		diceList.add(firstDice);
		
		Dice secondDice = new DiceImpl();
		secondDice.setMovement(movement);
		secondDice.setValue(getDiceNumber());
		diceDao.save(secondDice);
		diceList.add(secondDice);
		
		if (firstDice.getValue() == secondDice.getValue()) {
			Dice thirdDice = new DiceImpl();
			thirdDice.setMovement(movement);
			thirdDice.setValue(firstDice.getValue());
			diceDao.save(thirdDice);
			diceList.add(thirdDice);

			Dice fourthDice = new DiceImpl();
			fourthDice.setMovement(movement);
			fourthDice.setValue(firstDice.getValue());
			diceDao.save(fourthDice);
			diceList.add(fourthDice);
		}

		return assembly(diceList);
	}

	private byte getDiceNumber() {
		Random generator = new Random();
		return (byte) (generator.nextInt(6) + 1);
	}

	@Override
	public void save(Dice dice) {
		diceDao.save(dice);
	}
}
