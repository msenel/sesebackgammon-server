package service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import model.Checker;
import model.Player;
import model.Point;
import model.dao.CheckerDao;
import model.dto.CheckerDto;
import model.impl.CheckerImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.CheckerService;
import service.PlayerService;
import service.PointService;

@Service
public class CheckerServiceImpl implements CheckerService {

	@Autowired
	private CheckerDao checkerDao;
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private PointService pointService;

	@Override
	public Checker createChecker(Player player, Point point) {
		Checker checker = new CheckerImpl();
		checker.setPlayer(player);
		checker.setPoint(point);
		
		checkerDao.save(checker);
		
		return checker;
	}

	@Override
	public CheckerDto assembly(Checker checker) {
		CheckerDto checkerDto = new CheckerDto();
		checkerDto.setId(checker.getId());
		checkerDto.setInternId(checker.getInternId());
		checkerDto.setPlayer(playerService.assembly(checker.getPlayer()));
		return checkerDto;
	}

	@Override
	public List<CheckerDto> assembly(Set<Checker> checkerList) {
		
		List<CheckerDto> checkerDtoList = new ArrayList<CheckerDto>();
		for (Checker checker : checkerList) {
			checkerDtoList.add(assembly(checker));
		}
		return checkerDtoList;
	}

	@Override
	public Checker save(Checker checker) {
		checkerDao.save(checker);
		return checker;
	}
}
