package service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import model.Game;
import model.Player;
import model.User;
import model.dao.PlayerDao;
import model.dto.PlayerDto;
import model.impl.PlayerImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.PlayerService;
import service.UserService;
import util.Color;

@Service
public class PlayerServiceImpl implements PlayerService {
	
	@Autowired
	private PlayerDao playerDao;

	@Autowired
	private UserService userService;
	
	
	@Override
	public Player createPlayer(Game game, User user, Color color) {
		Player player = new PlayerImpl();
		player.setColor(color);
		player.setUser(user);
		player.setGame(game);
		
		playerDao.save(player);
		
		return player;
	}

	@Override
	public PlayerDto assembly(Player player) {
		PlayerDto playerDto = new PlayerDto();
		playerDto.setId(player.getId());
		playerDto.setColor(player.getColor());
		playerDto.setUser(userService.assembly(player.getUser()));
		return playerDto;
	}

	@Override
	public List<PlayerDto> assembly(Set<Player> playerList) {
		List<PlayerDto> playerDtoList = new ArrayList<PlayerDto>();
		for (Player player : playerList) {
			playerDtoList.add(assembly(player));
		}
		return playerDtoList;
	}

	@Override
	public void delete(Player player) {
		playerDao.delete(player);
	}

	@Override
	public Player getPlayer(Game game, User user) {
		return playerDao.get(game, user);
	}

	@Override
	public void update(Player player) {
		playerDao.save(player);
	}

}
