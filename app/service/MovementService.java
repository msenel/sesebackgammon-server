package service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import model.Game;
import model.Movement;
import model.Player;
import model.User;
import model.dto.MovementDto;
import service.impl.exception.ForbiddenMoveException;

public interface MovementService {
	public List<MovementDto> assembly(Set<Movement> movementList);
	public MovementDto assembly(Movement movement);
	public void move(User user, long gameId, byte sourcePointInternalId,
			byte targetPointInternalId) throws ForbiddenMoveException;
	public Movement initializeMovement(Player player, Game game, Date created);
	public Movement getLastMovement(Game game);
}
