package model.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import model.Dice;
import model.dao.AbstractHibernateDao;
import model.dao.DiceDao;
import model.impl.DiceImpl;

@Repository
@Transactional
public class DiceDaoImpl extends AbstractHibernateDao implements DiceDao {

	@Override
	public void save(Dice dice) {
		getSession().saveOrUpdate(dice);
	}

	@Override
	public Dice get(long id) {
		return (Dice) getSession().get(DiceImpl.class, id);
	}

	@Override
	public void delete(Dice dice) {
		getSession().delete(dice);
	}

}
