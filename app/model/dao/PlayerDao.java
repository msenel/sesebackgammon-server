package model.dao;

import util.Color;
import model.Game;
import model.Player;
import model.User;

public interface PlayerDao {

	public void save(Player player);
	public Player get(long id);
	public void delete(Player player);
	public Player get(Game game, Color color);
	public Player get(Game game, User user);
}
