package model.dao;

import java.util.List;

import model.Game;
import model.User;

public interface GameDao {

	public void save(Game game);
	public Game get(long id);
	public void delete(Game game);
	public List<Game> getOpenGames();
	public List<Game> getOpenGames(User user);
	public List<Game> getMyGames(User user);
}
