package model.dao;

import model.Dice;

public interface DiceDao {
	public void save(Dice dice);
	public Dice get(long id);
	public void delete(Dice dice);
}
