package model.dao.mongodb;

import model.User;
import model.dao.UserDao;
import model.impl.UserImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository("mongoUserDao")
public class UserDaoImpl implements UserDao {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	
	@Override
	public void save(User user) {
		user.setId(213213213L);
		if (!mongoTemplate.collectionExists(UserImpl.class)) {
			mongoTemplate.createCollection(UserImpl.class);
		}
		mongoTemplate.insert(user);		
	}

	@Override
	public User get(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUser(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(User user) {
		// TODO Auto-generated method stub
		
	}

}
