package model.dao;

import model.Checker;

public interface CheckerDao {

	public void save(Checker checker);
	public Checker get(long id);
	public void delete(Checker checker);

}
