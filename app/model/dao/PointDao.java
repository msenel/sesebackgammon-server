package model.dao;

import model.Board;
import model.Point;

public interface PointDao {

	public void save(Point point);
	public Point get(long id);
	public void delete(Point point);
	public Point get(Board board, byte internId);
}
