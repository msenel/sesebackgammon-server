package model;

import util.Color;

public interface Player {

	public long getId();
	public void setId(long id);
	public User getUser();
	public void setUser(User user);
	public Game getGame();
	public void setGame(Game game);
	public Color getColor();
	public void setColor(Color color);
}
