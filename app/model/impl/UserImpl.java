package model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

import model.User;
import play.data.validation.Constraints;


@Document(collection = "users")
@Entity(name = "user")
@Table(name = "user")
public class UserImpl implements User{

	@org.springframework.data.annotation.Id
	@Id
	@GeneratedValue
	@Column(name = "user_id")
	public Long id;

	@Column(nullable=false)
	public String username;

	@Constraints.Email
	@Column(nullable=false)
	public String email;
	
	@Constraints.MinLength(value=4)
	@Column(nullable=false)
	public String password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
